<!DOCTYPE html>
<html lang="en">
<head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/style.css">
        <link rel="icon" href="./img/stroopwafel.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>The Amazing Stroopwafel</title>
    </head>
    <body>
        <!-- Start Header -->
        <header>
        <!-- Start Navbar -->
            <nav>
                <ul>
                    <img class="navimg" src="./img/stroopwafel.png" alt="">
                    <li><a href="./index.php">Homepage</a></li>
                    <li><a href="./activities.php">Activiteiten</a></li>
                    <li><a href="./information.php">Informatie</a></li>
                    <li><a href="./review.php">Recensies</a></li>
                    <li><a href="./contact.php">Contact</a></li>
                    <li><a href="./read.php">Je gegevens</a></li>
                </ul>
            </nav>
        <!-- End Navbar -->
        </header>
        -<div class="contact-row">
     <div class="contactinfo-col">
            <img src="" alt="">
            <img src="./img/imgcontactpagina.jpg" class="imgcontactpagina" alt="">
             <div class="contact-inner">
                <h3>Contact informatie<h3>
                <h2>Contact informatie</h2>
                <h4>Siroopwafelfabriek.nl</h4>
                <p>E-mail: info@siroopwafelfabriek.nl</p>
                <p>Telefoon: 0182 634 965</p><br>
                <h4>Stroopwafelvanmarkus.nl</h4>
                <p>E-mail: info@stroopwafelsvanmarkus.nl</p>
                <p>Telefoon: +31 (0)182 61 88 24</p><br>
                <h4>woltjesbakkerij.nl</h4>
                <p>E-mail: info@woltjesbackerij.nl</p>
                <p>Telefoon: 0299-402709</p>
             </div>
         </div>

<div class="container">
     <div class="contactfillout-col">
         <div class="contactfillout-inner">
         <form action="create.php" method="post">
            <h2>Contact invulformulier</h2>
            <table>
                <tr>
                  <td><label for="inputFirstname">Voornaam</label></td>
                  <td><label class="lastname" for="inputLastname">Achternaam</label></td>
                </tr>
                <tr>
                  <td><input type="text" class="input-name" name="firstname" id="inputFirstname" placeholder="Verplicht invullen"></td>
                  <td><input type="text" class="input-name" name="lastname" id="inputLastname" placeholder="Verplicht invullen"></td>
                </tr>
            </table>
               <br>
                  <label for="inputEmail">Email adres</label><br>
                  <input type="text" class="input-contact" name="email" id="inputEmail" placeholder="Vul in"><br><br>
 
            <label for="inputFirstname" class="form-label">Voornaam</label>
            <input type="text" name="firstname" id="inputFirstname" placeholder="Vul in">

            <label for="inputLastname" class="form-label">Achternaam</label>
            <input type="text" name="lastname" id="inputLastname" placeholder="Vul in">

            <label for="inputEmail">Email adress</label>
            <input type="text" name="email" id="inputEmail" placeholder="Verplicht">
                  <label for="inputMessage">Bericht</label><br>
                 <textarea type="text" class="input-contact" name="bericht" id="inputMessage" placeholder="Bericht..."></textarea><br><br>
 
            <label for="inputMessage">Bericht</label>
            <input type="text" name="message" id="inputMessage">

            <input type="submit">
                <input type="submit">
          </form>
         </div>
       </div>
    </div>
 </div>
</div>
 
 </body>
 </html>