<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="icon" href="./img/stroopwafel.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>The Amazing Stroopwafel</title>
</head>
<body>
<header>
  <!-- Start Navbar -->
        <nav>
          <ul>
             <img class="navimg" src="./img/stroopwafel.png" alt="">
               <li><a href="./index.php">Homepage</a></li>
               <li><a href="./activities.php">Activiteiten</a></li>
               <li><a href="./information.php">Informatie</a></li>
               <li><a href="./review.php">Recensies</a></li>
               <li><a href="./contact.php">Contact</a></li>
               <li><a href="./read.php">Je gegevens</a></li>
          </ul>
        </nav>
   <!-- End Navbar -->
 </header>
 <div class="information-row">
        <div class="information-col1">
           <div class="information-inner">
              <h3>Het ontstaan van de Goudse stroopwafel</h3>
                      <p>De stroopwafel werd voor het eerst gemaakt in het begin van de negentiende eeuw in Gouda, vandaar dat dit type koek als Goudse wafel bekend is. <br>
                         Stroopwafels werden in die tijd gemaakt van oude koeksnippers, deegresten en stroop en waren daardoor erg goedkoop.
                         Ze werden in de 19e eeuw dan ook wel armenkoeken genoemd.<br>
                         Vanaf 1870 werden de Goudse stroopwafels niet alleen in Gouda, maar ook elders vervaardigd.
                         Stroopwafels worden ook buiten Nederland gegeten. Ze zijn vaak in supermarkten te koop of in speciale winkels. Soms zijn de stroopwafels in het buitenland kleiner en duurder dan in Nederland.</p>


           </div>
          </div>            
            <div class="informationimg-col1">
               <img src="./img/imginformationpage1.jpg" class="informationimg" alt="">
          </div>
 </div>
    <div class="information-row2">
     <div class="informationimg-col2">
      <img src="./img/informationpageimg3.png" class="informationimg" alt="">                       
    </div>
     <div class="information-col2">
      <div class="information-inner">
       <h3>Bereidingswijzen</h3>  
       <p>Meng de lauwwarme melk met de gedroogde gist. 
          Doe de bloem, boter, basterdsuiker, het ei en zout in een kom en meng samen met het gistmengsel door tot een soepel deeg. 
          Dek de kom af met plasticfolie en laat het deeg op een warme, tochtvrije plek 1 uur rijzen.
          <p>
          Doe voor de vulling de stroop in een steelpan en verwarm deze al roerend op middelhoog vuur. 
          Voeg de lichtbruine basterdsuiker en de boter toe en breng het mengsel al roerend aan de kook. 
          Laat de stroop 1-2 minuten doorkoken en blijf roeren, totdat alle suikerkristallen opgelost zijn. 
          Haal de pan van het vuur en breng de stroop eventueel op smaak met kaneel.  </p>                                 
     </div>
  </div>
</div>                   
  <div class="information-row3">
      <div class="information-col3">
          <div class="information-inner">
          <h3>Ingrediënten</h3>
                  <p>Voor het stroopwafeldeeg
                      <p>
                    30 ml melk, lauwwarm
                    10 gr gedroogde gist
                    300 gr bloem
                    150 gr boter, op kamertemperatuur
                    90 gr witte basterdsuiker
                    1 ei, op kamertemperatuur
                    ¼ tl zout
                    <p>
                    Voor de stroopvulling
                    <p>
                    200 gr keukenstroop
                    100 gr lichtbruine basterdsuiker
                    75 gr boter
                    ½ tl kaneelpoeder, optioneel
                    <p>
                    Verder nodig
                    <p>
                    boter of neutrale olie, om in te vetten
      </div>
 </div>            
     <div class="informationimg-col1">
        <img src="./img/informationpageimg2.jpg" class="informationimg" alt="">
     </div>
   </div>
</div>

<footer>Copyright &copy; Tess Jansen & Claudia Nijholt - 2021</footer>

<script src="./js/script.js"></script>
</body>
</html>