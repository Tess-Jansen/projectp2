<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Stroopwafels</title>
</head>
<body>
    <!-- Start Header -->
    <header>
        <nav>
            <ul>
                <img class="navimg" src="./img/stroopwafel.png" alt="">
                <li><a href="./index.php">Homepage</a></li>
                <li><a href="./activities.php">Activiteiten</a></li>
                <li><a href="./information.php">Informatie</a></li>
                <li><a href="./review.php">Recensies</a></li>
                <li><a href="./contact.php">Contact</a></li>
                <li><a href="./read.php">Je gegevens</a></li>
            </ul>
        </nav>
    <!-- End Navbar -->
    </header>
    <!-- End Header -->

    <!-- Start Main -->
    <div class="review-row1">
        <div class="review-col1">
            <div class="review-inner1">
                <h3>Laat hier je recensie achter!</h3>
                <p>Wij vragen mensen om recensies achter te laten over hun ervaringen met stroopwafels. Zo kunnen mensen met twijfels over het proberen van de stroopwafels, die twijfels eindelijk op zij kunnen zetten en die grote stap kunnen maken.
                <br>
                <br>
                Help jij deze mensen ook om hun twijfels aan de kan te zetten? laat nu een recensie achter!
                </p>
            </div>
        </div>
        <div class="review-col2">
            <div class="review-inner2">
                <br>
                <p>
                    &#9733;&#9733;&#9733;&#9733;&#9733;
                    <br>
                    <i>"Yammie!"</i>
                    <br>
                    <br>
                    <i>-Loes</i>
                </p>
                <br>                
            </div>
            <div class="review-inner2">
                <br>
                <p>
                    &#9733;&#9733;&#9733;&#9733;&#9733;
                    <br>
                    <i>"5 sterren"</i>
                    <br>
                    <br>
                    <i>-Daimy</i>
                </p>
                <br> 
            </div>
            <div class="review-inner2">
                <br>
                <p>
                    &#9733;&#9733;&#9733;&#9733;&#9733;
                    <br>
                    <i>"Ik eet ze elke zaterdag"</i>
                    <br>
                    <br>
                    <i>-Janice</i>
                </p>
                <br> 
            </div>
        </div>
    </div>
    <div class="review-row2">
        <div class="review-col202">
            <div class="review-inner3">
                <form action="./create.php" method="post">
                    <h2>Laat hier je recensie achter!</h2>

                    <table>
                        <tr>
                            <td><label for="inputFirstname">Voornaam: </label></td>
                            <td><label for="inputLastname">Achternaam: </label></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="firstname" id="inputFirstname" placeholder="Verplicht"></td>
                            <td><input type="text" name="lastname" id="inputLastname" placeholder="Verplicht"></td>
                        </tr>
                    </table>
                    <label for="inputemail">Email: </label><br>
                    <input class="input-review" type="text" name="email" id="inputEmail" placeholder="Verplicht"><br><br>

                    <label for="inputMessage">Recensie: </label><br>
                    <textarea class="input-review" type="text" name="recensie" id="inputMessage" placeholder="Maximaal 150 karakters"></textarea>

                    <input type="submit">
                </form>
            </div>
        </div>
    </div>
    <!-- End Main -->

    <!-- Start Footer -->
    <footer>Copyright &copy; Tess Jansen & Claudia Nijholt - 2021</footer>
    <script src="./js/script.js"></script>
</body>
</html>