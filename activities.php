<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Stroopwafels</title>
</head>
<body>
    <!-- Start Header -->
    <header>
<!-- Start Navbar -->
        <nav>
            <ul>
                <img class="navimg" src="./img/stroopwafel.png" alt="">
                <li><a href="./index.php">Homepage</a></li>
                <li><a href="./activities.php">Activiteiten</a></li>
                <li><a href="./information.php">Informatie</a></li>
                <li><a href="./review.php">Recensies</a></li>
                <li><a href="./contact.php">Contact</a></li>
                <li><a href="./read.php">Je gegevens</a></li>
            </ul>
        </nav>
    <!-- End Navbar -->
    </header>
    <!-- End Header -->

    <!-- Start Main -->
    <div class="activities-row">
        <div class="menu-col">
        <div class="sidebar">
                <h2>Verschillende activiteiten</h2>
                <hr>
                <ul>
                    <li><a href="#DeSiroopwafelfabriek">De Siroopwafelfabriek</a></li>
                    <br>
                    <br>
                    <li><a href="#Markus&Markus">Markus & Markus</a></li>
                    <br>
                    <br>
                    <li><a href="#WoltjesBakkerij">Woltjes Bakkerij</a></li>
                </ul>
            </div>
        </div>
        <div class="activities-col">
            <div class="activities-inner" id="DeSiroopwafelfabriek">
                <img src="./img/activieit1.png" alt="">
                <h2>De Siroopwafelfabriek</h2>
                <p>Ontdek het verhaal achter Kamphuisen en stap zelf de wondere wereld van de Siroopwafelfabriek binnen. Eenmaal binnen komt de heerlijke, zoete geur van Kamphuisen siroopwafels je tegemoet en kun je zelf ervaren hoe de lekkerste siroopwafels van Gouda gemaakt worden. De Siroopwafelfabriek is gevestigd in het oude centrum van Gouda, op de Markt.
                    <br>
                    <br>
                    De Kamphuisen Siroopwafel is door de eeuwen heen een grote inspiratiebron geweest voor veel andere stroopwafelbakkers. De siroopwafels waren anders dan de stroopwafels en vielen op door de krokante koek en de smaakvolle karamelsiroop die ertussen zat. Veel bakkers deden ijverig hun best om de wafel na te maken. Met weinig succes. Het recept is door de jaren heen zorgvuldig geheim gehouden.
                    <br>
                    <br>
                    Kamphuisen Siroopwafels zijn heerlijke, krokante siroopwafels gebakken volgens het originele Goudse recept van de Kamphuisen Siroopwafel uit 1810. De wafels worden vijf dagen per week vers gebakken en verpakt in folie zodat ze zo lang mogelijk hun knapperigheid behouden. Wil jij deze lekkere siroopwafels cadeau doen of ze zelf komen bakken in onze fakbriek?
                </p>
            </div>
            <div class="activities-inner" id="Markus&Markus">
                <img src="./img/activiteit2.png" alt="">
                <h2>Markus & Markus</h2>
                <p>Bij Markus & Markus maken we de lekkerste stroopwafel van Nederland. Dat zeggen wij niet alleen, maar iedereen die 'm heeft geproefd. Onze stroopwafels hebben een lekkere krokante koek met daartussen een heerlijk zachte stroop.
                    <br>
                    <br>
                    Onze stroopwafels zijn heerlijk krokant en zijn gevuld met een lekker zachte stroop. Onze ervaring is dan ook dat iedereen u graag jaarlijks terug ziet komen met onze verse Goudse stroopwafels. Veel scholen, kerken en verenigingen die geldinzamelingsacties op touw zetten, kiezen dan ook voor onze verse Markus stroopwafels voor huis-aan-huis verkoop of verkoping tijdens een goede doelen markt of braderie. Markus & Markus ondersteunt de stroopwafelacties graag door de stroopwafels aan te bieden tegen een gunstige inkoopprijs. Een groot deel van de opbrengst kunt u dus al direct reserveren voor het goede doel of om de clubkas te spekken!
                </p>
            </div>
            <div class="activities-inner" id="WoltjesBakkerij">
                <img src="./img/activiteit3.png" alt="">
                <h2>Woltjes Bakkerij</h2>
                <p>
                Midden op de dijk vindt u de Woltjes, een authentieke stroopwafel bakkerij, Woltjes museum, en de winkel van Woltjes. Dagelijks kunt u hier demonstraties en proeverijen bijwonen. Een must see als u in Volendam bent.
                <br>
                <br> 
                Als u een demonstratie bijwoont van Woltjes komt u niet alleen aan de weet hoe stroopwafels worden gemaakt, maar ook hoe ze proeven. In de backerij van Woltje is volop gelegenheid tot het proeven van deze oer Hollandse lekkerij.
                <br>
                <br>
                Als u Volendam bezoekt is het een absolute aanrader om eens langs te gaan bij Woltje’s winkel. Hier vindt u een keur aan Hollandse lekkernijen zoals stroopwafels, chocolade, speculaas, ijs en souvenir. Wij heten u in ieder geval van harte welkom.</p>
            </div>
        </div>
    </div>
    
    <!-- End Main -->

    <!-- Start Footer -->
        <footer>Copyright &copy; Tess Jansen & Claudia Nijholt - 2021</footer>
    <!-- End Footer -->
    <script src="./js/script.js"></script>
</body>
</html>