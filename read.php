<?php
    include("./connect_db.php");
    $sql = "SELECT * FROM `recensie`";

    $result = mysqli_query($conn, $sql);

    $records = "";
    while ($record = mysqli_fetch_assoc($result)){
         $records .=    "<tr><th scope = 'row'>". $record['id'] . "</th>" .
                          "<td>" . $record['firstname'] . "</td>" .  
                          "<td>" . $record['lastname'] . "</td>" . 
                          "<td>" . $record['email'] . "</td>" .
                          "<td>" . $record['recensie'] . "</td>" .
                          "<td>
                              <a href = './update.php?id= " . $record["id"] .  "'>
                              <img src='./img/icons/b_edit.png' alt='pencil'>
                              </a>
                          </td>
                          <td>
                            <a href = './delete.php?id= " . $record["id"] .  "'>
                            <img src='./img/icons/b_drop.png' alt='cross'>
                            </a>
                          </td>
                        </tr>";
                    } 
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="icon" href="./img/stroopwafel.png">
    <title></title>
</head>
<body>
    <!-- Start Header -->
    <header>
        <!-- Start Navbar -->
        <nav>
            <ul>
                <img class="navimg" src="./img/stroopwafel.png" alt="">
                <li><a href="./index.php">Homepage</a></li>
                <li><a href="./activities.php">Activiteiten</a></li>
                <li><a href="./information.php">Informatie</a></li>
                <li><a href="./review.php">Recensies</a></li>
                <li><a href="./contact.php">Contact</a></li>
                <li><a href="./read.php">Je gegevens</a></li>
            </ul>
        </nav>
        <!-- End Navbar -->
    </header>
    <!-- End Header -->
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <table id="table">
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">voornaam</th>
                <th scope="col">achternaam</th>
                <th scope="col">email</th>
                <th scope="col">recensie</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php
                echo $records;
            ?>
        </tbody>
    </table>
</body>
</html>