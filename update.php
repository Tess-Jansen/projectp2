<?php
    $id = $_GET["id"];

    include("./connect_db.php");

    $sql = " SELECT * FROM `recensie` WHERE `id` = $id";

    $result = mysqli_query($conn, $sql);

    $record = mysqli_fetch_assoc($result);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="icon" href="./img/stroopwafel.png">
    <title></title>
</head>
<body>
        <!-- Start Header -->
        <header>
        <!-- Start Navbar -->
        <nav>
            <ul>
                <img class="navimg" src="./img/stroopwafel.png" alt="">
                <li><a href="./index.php">Homepage</a></li>
                <li><a href="./activities.php">Activiteiten</a></li>
                <li><a href="./information.php">Informatie</a></li>
                <li><a href="./review.php">Recensies</a></li>
                <li><a href="./contact.php">Contact</a></li>
            </ul>
        </nav>
        <!-- End Navbar -->
    </header>
    <!-- End Header -->
<div class="review-row2">
        <div class="review-col202">
            <div class="review-inner3">
                <form action="./update_script.php" method="post">
                    <h2>Laat hier je recensie achter!</h2>

                    <table>
                        <tr>
                            <td><label for="inputFirstname">Voornaam: </label></td>
                            <td><label for="inputLastname">Achternaam: </label></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="firstname" id="inputFirstname" aria-describedby= "firstnameHelp" value = "<?php echo $record["firstname"]; ?>"  placeholder="Verplicht"></td>

                            <td><input type="text" name="lastname" id="inputLastname" aria-describedby= "lastnameHelp" value = "<?php echo $record["lastname"]; ?>" placeholder="Verplicht"></td>
                        </tr>
                    </table>
                    <label for="inputemail">Email: </label><br>
                    <input class="input-review" type="text" name="email" id="inputEmail" aria-describedby= "emailHelp" value = "<?php echo $record["email"]; ?>" placeholder="Verplicht"><br><br>

                    <label for="inputMessage">Recensie: </label><br>
                    <textarea class="input-review" type="text" name="recensie" id="inputMessage" aria-describedby= "recensieHelp" value = "<?php echo $record["recensie"]; ?>" placeholder="Maximaal 150 karakters"></textarea>

                    <input type="hidden" value="<?php echo $id; ?>" name="id" >
                    <input type="submit">
                </form>
            </div>
        </div>
    </div>
    
</body>
</html>