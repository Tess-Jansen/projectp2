<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/style.css">
        <link rel="icon" href="./img/stroopwafel.png">
        <title>Stroopwafels</title>
    </head>
    <body>
        <!-- Start Header -->
        <header>
        <!-- Start Navbar -->
            <nav>
                <ul>
                    <img class="navimg" src="./img/stroopwafel.png" alt="">
                    <li><a href="./index.php">Homepage</a></li>
                    <li><a href="./activities.php">Activiteiten</a></li>
                    <li><a href="./information.php">Informatie</a></li>
                    <li><a href="./review.php">Recensies</a></li>
                    <li><a href="./contact.php">Contact</a></li>
                    <li><a href="./read.php">Je gegevens</a></li>
                </ul>
            </nav>
        <!-- End Navbar -->
        </header>
        <!-- End Header -->
        <!-- Start Section -->
            <div class="homepage-row">
                <div class="review">
                    <div class="review-inner">
                        <h3>Reviews over stroopwafels</h3>
                        <hr>
                        <p>
                            &#9733;&#9733;&#9733;&#9733;&#9733;
                            <br>
                            <i>"Stroopwafels zijn de lekkerste van de wereld."</i>
                            <br>
                            <br>
                            <i>-Helena</i>
                        </p>
                        <br>
                        <p>
                            &#9733;&#9733;&#9733;&#9733;&#9733;
                            <br>
                            <i>"Stroopwafels, de lekkerste die er maar zijn!"</i>
                            <br>
                            <br>
                            <i>-Windhoos Computers</i>
                        </p>
                        <br>
                        <p>
                            &#9733;&#9733;&#9733;&#9733;&#9733;
                            <br>
                            <i>"Superrrrr lekkere stroopwafelkruimels echt de lekkerste smaak die ik geproefd heb"</i>
                            <br>
                            <br>
                            <i>-Ilse</i>
                        </p>
                        <br>
                        <p>
                            &#9733;&#9733;&#9733;&#9733;&#9733;
                            <br>
                            <i>"Gewoon lekker"</i>
                            <br>
                            <br>
                            <i>-Erwin</i>
                        </p>
                    </div>
                </div>

                <div class="carousel-col">
                    <div class="carousel-inner">
                        <div id="carousel">
                            <div class="slider-item active">
                                <img src="./img/sliderfoto1.jpg" alt="">
                                <div class="slider-content">
                                    <h2>Stroopwafels</h2>
                                    <span><strong>Het meest veelzijdige stukje koek!</strong></span>
                                </div>
                            </div>
                            <div class="slider-item">
                                <img src="./img/sliderfoto2.jpg" alt="">
                                <div class="slider-content">
                                    <h2>Stroopwafel likeur</h2>
                                    <span><strong>Ja! Er is zelfs drank gemaakt van stroopwafels!</strong></span>
                                </div>
                            </div>
                            <div class="slider-item">
                                <img src="./img/sliderfoto3.jpg" alt="">
                                <div class="slider-content">
                                    <h2>Stroopwafelkruimels</h2>
                                    <span><strong>Ook alle stukjes stroopwafel die overblijven worden goed benut!</strong></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="intro-inner">
                        <h2><strong>Stroopwafels</strong></h2>
                        <p>Stroopwafels, Wie lust ze niet? Vele mensen eten ze al van jongs af aan. Ze zijn echt deel van de hollandse cultuur. Maar hoeveel weet je nou eigenlijk over stroopwafels? je kunt namelijk veel meer dan ze alleen maar opeten!</p>
                        <p class="end-intro">Op deze website vind je van alles over stroopwafels, kijk gerust even rond!</p>
                    </div>
                </div>
                    <div class="card-col">
                        <div class="card">
                            <img src="./img/activieit1.png" class="cardimg" alt="Avatar" style="width:100%">
                            <div class="container">
                                <h4><strong>De Siroopwafelfabriek</strong></h4> 
                                <p>Een bezoek aan de fabriek is een fantastische ervaring die je niet snel meer zal vergeten.</p>
                                <br>
                                <a href="./activities.php" class="button">Meer lezen</a>
                                <br>
                                <br>
                            </div>
                        </div>

                        <div class="card">
                            <img src="./img/activiteit2.png" class="cardimg" alt="Avatar" style="width:100%">
                            <div class="container">
                                <h4><strong>Markus & Markus Stroopwafels</strong></h4> 
                                <p>Bij Markus vind je alles wat je wilt weten over hun stroopwafel en hoe je op de lekkerste manier geld in kan zamelen!</p>
                                <br>
                                <a href="./activities.php" class="button">Meer lezen</a>
                                <br>
                                <br>
                            </div>
                        </div>

                        <div class="card">
                            <img src="./img/activiteit3.png" class="cardimg" alt="Avatar" style="width:100%">
                            <div class="container">
                                <h4><strong>Woltjes Bakkerij</strong></h4> 
                                <p> Je vindt er behalve de winkel ook de bakkerij zelf, waar je demonstraties kunt bijwonen.</p>
                                <br>
                                <a href="./activities.php" class="button">Meer lezen</a>
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <!-- End Section -->

    <!-- Start Footer -->

    <footer>Copyright &copy; Tess Jansen & Claudia Nijholt - 2021</footer>


    <!-- Start JS Script -->
    <script src="./js/script.js"></script>
    <!-- End JS Script -->
    </body>
</html>